///<reference path="../../tools/typings/tsd.d.ts" />
///<reference path="../../tools/typings/typescriptApp.d.ts" />

((): void => {

    angular.module(AppSettings.nameApplication, ['ui.router', 'uiGmapgoogle-maps', AppSettings.dependenciesModule])

        .config(['$stateProvider', '$urlRouterProvider', 'uiGmapGoogleMapApiProvider', ($stateProvider, $urlRouterProvider, uiGmapGoogleMapApiProvider): void => {
            uiGmapGoogleMapApiProvider.configure({
                key: 'AIzaSyATPx0Z-w2U9Ac1WDgIGW6awopLOh2JoII',
                v: '3.17',
                libraries: 'weather,geometry,visualization'});

            $urlRouterProvider.otherwise('/start');
            $stateProvider
                .state('start', {
                    cache: false,
                    url: '/start',
                    templateUrl: 'templates/start-template.html'
                })
                .state('map', {
                        url: '/map',
                        templateUrl: 'templates/map-template.html',
                        controller: 'App.controllers.mapController as mapController'
                    })
            ;
        }])
    ;
})();