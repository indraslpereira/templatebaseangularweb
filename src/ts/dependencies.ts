///<reference path="../../tools/typings/tsd.d.ts" />
///<reference path="../../tools/typings/typescriptApp.d.ts" />

module AppSettings {
    export var nameApplication: string = 'App';
    export var filtersModule: string = nameApplication + '.filters';
    export var controllersModule: string = nameApplication + '.controllers';
    export var servicesModule: string = nameApplication + '.services';
    export var factoryModule: string = nameApplication + '.factory';
    export var dependenciesModule: string = nameApplication + '.dependencies';

    angular.module(filtersModule, []);
    angular.module(controllersModule, []);
    angular.module(servicesModule, []);
    angular.module(factoryModule, []);

    angular.module(dependenciesModule, [
        filtersModule,
        controllersModule,
        servicesModule,
        factoryModule
    ]);
}