///<reference path="../../../tools/typings/tsd.d.ts" />
///<reference path="../../../tools/typings/typescriptApp.d.ts"/>

module AppControllers {
    'use strict';
    class MapController {
        data: any [


            ];
        map: any;
        static $inject = ['$http'];
        constructor($http) {
            //Controller Body
            this.data = [
                {
                    id:'1',
                    location: {
                        latitude: 4.801812, longitude: -75.692537
                    }
                },
                {
                    id:'2',
                    location: {
                        latitude: 4.803919,
                        longitude: -75.690318
                    }
                }
            ];
            this.map = { center: { latitude:  4.807326, longitude:  -75.692637 }, zoom: 14 };
        }
    }
    angular.module('App.controllers')
        .controller('App.controllers.mapController', MapController);
}

//For use inside routes definition app.ts:
//.state('map', {
//                        url: '/map',
//                        templateUrl: 'templates/map-template.html',
//                        controller: 'ASTApp.mapController as map'
//                    });

//For use inside template:
//    {{map.data}}

//Check dependencies inside app.ts
//    angular.module('App.controllers', []);
//    angular.module('app', ['ionic', 'App.controllers'])

//Check insertion of javascript file inside index.html
//<script src="js/app.js" type="application/javascript"></script>
//<script src="js/controllers/mapcontroller.js"></script>

